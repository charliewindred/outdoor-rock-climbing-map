package com.example.outdoorclimbingmap.services

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.example.outdoorclimbingmap.SQLiteHelper
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_MEDIA_REF_PATH
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.MEDIA_REF_TABLE
import com.example.outdoorclimbingmap.interfaces.IEntityService
import com.example.outdoorclimbingmap.model.MediaRefModel

class MediaRefService(context: Context) : IEntityService<MediaRefModel> {
    private val dbHelper = SQLiteHelper.getInstance(context)

    override fun getAll(): List<MediaRefModel> {
        val mediaRefList = ArrayList<MediaRefModel>()
        val sqlStatement = "SELECT * FROM $MEDIA_REF_TABLE"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val id: Int = cursor.getInt(0)
                val mediaPath: String = cursor.getString(1)
                val mediaRef = MediaRefModel(id, mediaPath)
                mediaRefList.add(mediaRef)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return mediaRefList
    }

    override fun getById(id: Int): MediaRefModel? {
        var mediaRef: MediaRefModel? = null
        val sqlStatement = "SELECT * FROM $MEDIA_REF_TABLE WHERE ID = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!){
            val mediaRefId: Int = cursor.getInt(0)
            val mediaPath: String = cursor.getString(1)
            mediaRef = MediaRefModel(mediaRefId, mediaPath)
        }
        cursor.close()
        db.close()

        return mediaRef
    }

    override fun add(model: MediaRefModel): MediaRefModel {
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_MEDIA_REF_PATH, model.mediaPath)

        model.id = db?.insert(MEDIA_REF_TABLE, null, cv)!!.toInt()
        db.close()

        return model
    }

    override fun delete(id: Int) {
        val sqlStatement = "DELETE FROM $MEDIA_REF_TABLE WHERE ID = $id;"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()
    }

    override fun update(model: MediaRefModel): MediaRefModel {
        val sqlStatement = "UPDATE $MEDIA_REF_TABLE" +
                "SET $COLUMN_MEDIA_REF_PATH = '${model.mediaPath}', " +
                "WHERE ID = ${model.id};"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()

        return model
    }
}