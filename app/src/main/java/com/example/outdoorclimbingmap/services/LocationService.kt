package com.example.outdoorclimbingmap.services

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.example.outdoorclimbingmap.SQLiteHelper
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_LOCATION_DESCRIPTION
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_LOCATION_DOCUMENT_ID
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_LOCATION_LATITUDE
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_LOCATION_LONGITUDE
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_LOCATION_NAME
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.LOCATION_TABLE
import com.example.outdoorclimbingmap.interfaces.IEntityService
import com.example.outdoorclimbingmap.model.LocationModel

class LocationService(context: Context): IEntityService<LocationModel> {
    private val dbHelper = SQLiteHelper.getInstance(context)

    override fun getAll(): List<LocationModel> {
        val locations = ArrayList<LocationModel>()
        val sqlStatement = "SELECT * FROM " + LOCATION_TABLE
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val id: Int = cursor.getInt(0)
                val documentId: String? = cursor.getString(1)
                val name: String = cursor.getString(2)
                val description: String? = cursor.getString(3)
                val longitude: Double = cursor.getDouble(4)
                val latitude: Double = cursor.getDouble(5)
                val location = LocationModel(id, documentId, name, description, longitude, latitude,null)
                locations.add(location)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return locations
    }

    override fun getById(id: Int): LocationModel? {
        var location: LocationModel? = null
        val sqlStatement = "SELECT * FROM " + LOCATION_TABLE + " WHERE ID = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!){
            val documentId: String = cursor.getString(1)
            val name: String = cursor.getString(2)
            val description: String = cursor.getString(3)
            val longitude: Double = cursor.getDouble(4)
            val latitude: Double = cursor.getDouble(5)
            location = LocationModel(id, documentId, name, description, longitude, latitude,null)
        }
        cursor.close()
        db.close()

        return location
    }
//
    override fun add(model: LocationModel): LocationModel {
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_LOCATION_DOCUMENT_ID, model.documentId)
        cv.put(COLUMN_LOCATION_NAME, model.name)
        cv.put(COLUMN_LOCATION_DESCRIPTION, model.description)
        cv.put(COLUMN_LOCATION_LATITUDE, model.latitude)
        cv.put(COLUMN_LOCATION_LONGITUDE, model.longitude)

        model.id = db?.insert(LOCATION_TABLE, null, cv)!!.toInt()
        db.close()

        return model
    }

    override fun delete(id: Int) {
        val sqlStatement = "DELETE FROM " + LOCATION_TABLE + " WHERE ID = $id;"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()
    }

    override fun update(model: LocationModel): LocationModel {
        val sqlStatement = "UPDATE $LOCATION_TABLE " +
                "SET " + COLUMN_LOCATION_DOCUMENT_ID + " = '${model.documentId}', " +
                COLUMN_LOCATION_NAME + " = '${model.name}', " +
                COLUMN_LOCATION_DESCRIPTION + " = '${model.description}'," +
                COLUMN_LOCATION_LATITUDE + " = ${model.latitude}," +
                COLUMN_LOCATION_LONGITUDE + " = ${model.longitude}," +
                "WHERE ID = ${model.id};"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()

        return model
    }

    fun locationExists(name:String) : Boolean {
        val sqlStatement = "SELECT EXISTS(SELECT 1 FROM " + LOCATION_TABLE + " WHERE " + COLUMN_LOCATION_NAME + " = '${name}');"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor = db?.rawQuery(sqlStatement, null)!!
        cursor.moveToFirst()

        return if (cursor.getInt(0) == 1) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }
    }
}