package com.example.outdoorclimbingmap.services

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.example.outdoorclimbingmap.SQLiteHelper
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_MEDIA_LOCATION_ID
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_MEDIA_MEDIA_REF_ID
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_MEDIA_PROBLEM_ID
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.MEDIA_TABLE
import com.example.outdoorclimbingmap.interfaces.IEntityService
import com.example.outdoorclimbingmap.model.MediaModel
import com.example.outdoorclimbingmap.model.ProblemModel

class MediaService(context: Context) : IEntityService<MediaModel> {
    private val dbHelper = SQLiteHelper.getInstance(context)

    override fun getAll(): List<MediaModel> {
        val mediaList = ArrayList<MediaModel>()
        val sqlStatement = "SELECT * FROM $MEDIA_TABLE"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val id: Int = cursor.getInt(0)
                val mediaRefId: Int = cursor.getInt(1)
                val routeId: Int = cursor.getInt(2)
                val locationId: Int = cursor.getInt(3)
                val media = MediaModel(id, mediaRefId, routeId, locationId)
                mediaList.add(media)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return mediaList
    }

    override fun getById(id: Int): MediaModel? {
        var media: MediaModel? = null
        val sqlStatement = "SELECT * FROM $MEDIA_TABLE WHERE ID = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!){
            val mediaId: Int = cursor.getInt(0)
            val mediaRefId: Int = cursor.getInt(1)
            val routeId: Int = cursor.getInt(2)
            val locationId: Int = cursor.getInt(3)
            media = MediaModel(mediaId, mediaRefId, routeId, locationId)
        }
        cursor.close()
        db.close()

        return media
    }

    override fun add(model: MediaModel): MediaModel {
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_MEDIA_MEDIA_REF_ID, model.mediaRefId)
        cv.put(COLUMN_MEDIA_LOCATION_ID, model.locationId)
        cv.put(COLUMN_MEDIA_PROBLEM_ID, model.problemId)

        model.id = db?.insert(MEDIA_TABLE, null, cv)!!.toInt()
        db.close()

        return model
    }

    override fun delete(id: Int) {
        val sqlStatement = "DELETE FROM $MEDIA_TABLE WHERE ID = $id;"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()
    }

    override fun update(model: MediaModel): MediaModel {
        val sqlStatement = "UPDATE $MEDIA_TABLE" +
                "SET $COLUMN_MEDIA_MEDIA_REF_ID = '${model.mediaRefId}', " +
                "SET $COLUMN_MEDIA_LOCATION_ID = '${model.locationId}', " +
                "WHERE ID = ${model.id};"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()

        return model
    }

    fun getAllByLocationId(id: Int) : ArrayList<MediaModel> {
        val medias = ArrayList<MediaModel>()
        val sqlStatement = "SELECT * FROM " + MEDIA_TABLE + " WHERE " + COLUMN_MEDIA_LOCATION_ID + " = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val mediaId: Int = cursor.getInt(0)
                val mediaRefId: Int = cursor.getInt(1)
                val problemId: Int = cursor.getInt(2)
                val locationId: Int = cursor.getInt(3)
                val media = MediaModel(mediaId, mediaRefId, problemId, locationId)
                medias.add(media)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return medias
    }

    fun getAllByProblemId(id: Int) : ArrayList<MediaModel> {
        val medias = ArrayList<MediaModel>()
        val sqlStatement = "SELECT * FROM " + MEDIA_TABLE + " WHERE " + COLUMN_MEDIA_PROBLEM_ID + " = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val mediaId: Int = cursor.getInt(0)
                val mediaRefId: Int = cursor.getInt(1)
                val problemId: Int = cursor.getInt(2)
                val locationId: Int = cursor.getInt(3)
                val media = MediaModel(mediaId, mediaRefId, problemId, locationId)
                medias.add(media)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return medias
    }
}