package com.example.outdoorclimbingmap.services

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.example.outdoorclimbingmap.SQLiteHelper
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_PROBLEM_DESCRIPTION
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_PROBLEM_GRADE
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_PROBLEM_LOCATION_ID
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.COLUMN_PROBLEM_NAME
import com.example.outdoorclimbingmap.SQLiteHelper.Companion.PROBLEM_TABLE
import com.example.outdoorclimbingmap.interfaces.IEntityService
import com.example.outdoorclimbingmap.model.ProblemModel

class ProblemService(context: Context): IEntityService<ProblemModel> {
    private val dbHelper = SQLiteHelper.getInstance(context)

    override fun getAll(): List<ProblemModel> {
        val problems = ArrayList<ProblemModel>()
        val sqlStatement = "SELECT * FROM $PROBLEM_TABLE"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val id: Int = cursor.getInt(0)
                val name: String = cursor.getString(1)
                val description: String = cursor.getString(2)
                val grade: String = cursor.getString(3)
                val locationId: Int = cursor.getInt(5)
                val problem = ProblemModel(id, null, name, description, grade,null, locationId, null)
                problems.add(problem)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return problems
    }

    override fun getById(id: Int): ProblemModel? {
        var problem: ProblemModel? = null
        val sqlStatement = "SELECT * FROM $PROBLEM_TABLE WHERE ID = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!){
            val problemId: Int = cursor.getInt(0)
            val name: String = cursor.getString(1)
            val description: String = cursor.getString(2)
            val grade: String = cursor.getString(3)
            val locationId: Int = cursor.getInt(4)
            problem = ProblemModel(problemId,null, name, description, grade,null, locationId, null)
        }
        cursor.close()
        db.close()

        return problem
    }

    override fun add(model: ProblemModel): ProblemModel {
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cv = ContentValues()

        cv.put(COLUMN_PROBLEM_NAME, model.name)
        cv.put(COLUMN_PROBLEM_DESCRIPTION, model.description)
        cv.put(COLUMN_PROBLEM_GRADE, model.grade)
        cv.put(COLUMN_PROBLEM_LOCATION_ID, model.offlineLocationId)

        model.id = db?.insert(PROBLEM_TABLE, null, cv)!!.toInt()
        db.close()

        return model
    }

    override fun delete(id: Int) {
        val sqlStatement = "DELETE FROM $PROBLEM_TABLE WHERE ID = $id;"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()
    }

    override fun update(model: ProblemModel): ProblemModel {
        val sqlStatement = "UPDATE $PROBLEM_TABLE" +
                "SET $COLUMN_PROBLEM_NAME = '${model.name}', " +
                "$COLUMN_PROBLEM_DESCRIPTION = '${model.description}'," +
                "$COLUMN_PROBLEM_GRADE = ${model.grade}," +
                "$COLUMN_PROBLEM_LOCATION_ID = ${model.offlineLocationId}," +
                "WHERE ID = ${model.id};"
        val db: SQLiteDatabase? = dbHelper?.writableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        cursor?.close()
        db?.close()

        return model
    }

    fun getAllByLocationId(id: Int) : ArrayList<ProblemModel> {
        val problems = ArrayList<ProblemModel>()
        val sqlStatement = "SELECT * FROM " + PROBLEM_TABLE + " WHERE $COLUMN_PROBLEM_LOCATION_ID = $id"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor? = db?.rawQuery(sqlStatement,null)

        if(cursor?.moveToFirst()!!)
            do {
                val problemId: Int = cursor.getInt(0)
                val name: String = cursor.getString(1)
                val description: String = cursor.getString(2)
                val grade: String = cursor.getString(3)
                val offlineLocationId: Int = cursor.getInt(4)
                val problem = ProblemModel(problemId, null, name, description, grade, null, offlineLocationId, null)
                problems.add(problem)
            } while(cursor.moveToNext())

        cursor.close()
        db.close()

        return problems
    }

    fun problemExists(name:String) : Boolean {
        val sqlStatement = "SELECT EXISTS(SELECT 1 FROM " + PROBLEM_TABLE + " WHERE " + COLUMN_PROBLEM_NAME + " = '${name}');"
        val db: SQLiteDatabase? = dbHelper?.readableDatabase
        val cursor: Cursor = db?.rawQuery(sqlStatement, null)!!
        cursor.moveToFirst()

        return if (cursor.getInt(0) == 1) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }
    }
}