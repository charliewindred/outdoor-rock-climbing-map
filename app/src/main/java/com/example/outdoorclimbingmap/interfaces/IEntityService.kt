package com.example.outdoorclimbingmap.interfaces

interface IEntityService<T> {
    fun getAll() : List<T>
    fun getById(id: Int) : T?
    fun add(model: T): T
    fun delete(id: Int)
    fun update(model: T) : T

}