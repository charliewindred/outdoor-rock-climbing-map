package com.example.outdoorclimbingmap

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.LinearLayout
import java.util.*
import kotlin.collections.ArrayList

class GalleryPagerAdapter(context: Context, private val imageUris: ArrayList<Uri>?,
                          private val imageViews: ArrayList<ImageView>?) : PagerAdapter() {
    private var mLayoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        if(imageUris != null){
            return imageUris.size
        }
        return imageViews?.size!!

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView: View = mLayoutInflater.inflate(R.layout.item, container, false)
        val imageViewMain = itemView.findViewById<View>(R.id.imageViewMain) as ImageView

        if(imageUris != null) {
            imageViewMain.setImageURI(imageUris[position])
        } else if(imageViews != null){
            imageViewMain.setImageDrawable(imageViews[position].drawable)
        }
        container.setBackgroundColor(Color.LTGRAY)
        Objects.requireNonNull(container).addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }
}