package com.example.outdoorclimbingmap.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.outdoorclimbingmap.R
import com.example.outdoorclimbingmap.services.MediaRefService
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.widget.Toast
import android.app.ProgressDialog
import com.example.outdoorclimbingmap.activity.MapsActivity
import com.example.outdoorclimbingmap.model.LocationModel
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.storage.*
import androidx.viewpager.widget.ViewPager
import com.example.outdoorclimbingmap.GalleryPagerAdapter


class AddLocationFragment : Fragment(), AdapterView.OnItemClickListener, OnMapReadyCallback {

    private lateinit var mediaRefService: MediaRefService
    private lateinit var storage: FirebaseStorage
    private lateinit var storageReference: StorageReference
    private lateinit var mapView: MapView
    private lateinit var selectedLocation : LatLng

    private var imageUris = ArrayList<Uri>()
    private var documentId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_location, container, false)
        val lat = requireArguments().getDouble("lat")
        val long = requireArguments().getDouble("long")
        selectedLocation = LatLng(lat,long)
        mediaRefService = MediaRefService(view.context)
        storage = FirebaseStorage.getInstance()
        storageReference = storage.reference
        mapView = view.findViewById(R.id.locationMiniMap) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        val btnAddImages = view.findViewById<Button>(R.id.btnAddImages)
        btnAddImages.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            startActivityForResult(Intent.createChooser(intent, "Select Images"), 1)
        }

        val buttonSubmit = view.findViewById<Button>(R.id.buttonSubmit)
        buttonSubmit.setOnClickListener {
            val name = view.findViewById<EditText>(R.id.editTextLocationName)
            val desc = view.findViewById<EditText>(R.id.editTextLocationDesc)

            when {
                name.text.length < 2 || name.text.length > 30 -> {
                    name.error = "Name should be between 2 and 30 characters."
                }
                desc.text.length < 2 || desc.text.length > 150 -> {
                    desc.error = "Description should be between 2 and 150 characters."
                }
                else -> {
                    val data = hashMapOf<String,Any>(
                        "name" to name.text.toString(),
                        "description" to desc.text.toString(),
                        "latitude" to lat,
                        "longitude" to long,
                        "imageCount" to imageUris.size
                    )
                    addLocationToFirestore(data)
                }
            }
        }
        return view
    }

    private fun uploadImage(uri: Uri, galleryIndex: Int, documentId: String) {
        val progressDialog = ProgressDialog(view?.context)
        progressDialog.setTitle("Uploading...")
        progressDialog.show()
        // Defining the child of storageReference
        val ref = storageReference.child("images/" + "location_" + documentId + "_" + galleryIndex)
        ref.putFile(uri)
            .addOnSuccessListener {
                progressDialog.dismiss()
                Toast.makeText(view?.context, "Upload Successful", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e ->
                progressDialog.dismiss()
                Toast.makeText(view?.context, "Failed " + e.message, Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener {
                val taskSnapshot = it
                val progress: Double =
                    (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
                progressDialog.setMessage("Uploaded " + progress.toString() + "%")
            }
    }

    private fun addLocationToFirestore(data: HashMap<String, Any>) {
        val db = Firebase.firestore
        db.collection("location")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    "ADD_LOCATION_FRAGMENT",
                    "DocumentSnapshot written with ID: ${documentReference.id}"
                )
                documentId = documentReference.id
                Toast.makeText(view?.context, "Location submitted successfully.", Toast.LENGTH_SHORT).show()
                (activity as MapsActivity).addMarker(LocationModel(
                    null, documentId,
                    data["name"] as String,
                    data["description"] as String,
                    data["latitude"] as Double,
                    data["longitude"] as Double,
                    data["imageCount"] as Int,
                ))

                if (imageUris.size > 0) {
                    for (i in 0 until imageUris.size) {
                        uploadImage(imageUris[i], i, documentReference.id)
                    }
                }
            }
            .addOnFailureListener { e ->
                Log.w("ADD_LOCATION_FRAGMENT", "Error adding document", e)
                Toast.makeText(
                    view?.context,
                    "Error. Please check your internet connection.",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == RESULT_OK && requestCode == 1) {
            val selectedImageUri = intent?.data
            if (selectedImageUri != null) {
                imageUris.add(selectedImageUri)
            }
            val gallery = view?.findViewById<View>(R.id.gallery) as ViewPager
            val adapter = GalleryPagerAdapter(requireView().context, imageUris, null)
            gallery.adapter = adapter

        }
    }

    companion object {
        fun newInstance(coordinates: LatLng): AddLocationFragment {
            val args = Bundle()
            args.putDouble("lat", coordinates.latitude)
            args.putDouble("long", coordinates.longitude)
            val fragment = AddLocationFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    override fun onMapReady(mMap: GoogleMap) {
        mMap.addMarker(MarkerOptions().position(selectedLocation))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedLocation, MapsActivity.DEFAULT_ZOOM.toFloat()))
        mapView.onResume()
    }
}