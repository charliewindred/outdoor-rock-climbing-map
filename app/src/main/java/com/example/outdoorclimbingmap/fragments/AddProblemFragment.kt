package com.example.outdoorclimbingmap.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.outdoorclimbingmap.R
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.widget.Toast
import android.app.ProgressDialog
import androidx.viewpager.widget.ViewPager
import com.example.outdoorclimbingmap.GalleryPagerAdapter
import com.example.outdoorclimbingmap.model.LocationModel
import com.google.firebase.storage.*

class AddProblemFragment : Fragment(), AdapterView.OnItemClickListener {

    private lateinit var storage: FirebaseStorage
    private lateinit var storageReference: StorageReference
    private lateinit var problemDocumentId: String

    private var imageUris = ArrayList<Uri>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add_problem, container, false)
        storage = FirebaseStorage.getInstance()
        storageReference = storage.reference
        var locationModel = requireArguments().getSerializable("locationModel")!! as LocationModel

        val btnAddImages = view.findViewById<Button>(R.id.btnAddImages)
        btnAddImages.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            startActivityForResult(Intent.createChooser(intent, "Select Images"), 1)
        }

        val name = view.findViewById<EditText>(R.id.editTextProblemName)
        val grade = view.findViewById<EditText>(R.id.editTextProblemGrade)
        val desc = view.findViewById<EditText>(R.id.editTextProblemDesc)

        val buttonSubmit = view.findViewById<Button>(R.id.buttonSubmit)
        buttonSubmit.setOnClickListener {
            when {
                name.text.length < 2 || name.text.length > 30 -> {
                    name.error = "Name should be between 2 and 30 characters."
                }
                desc.text.length < 2 || desc.text.length > 150 -> {
                    desc.error = "Description should be between 2 and 150 characters."
                }
                grade.text.length < 2 || grade.text.length > 150 -> {
                    grade.error = "Grade should be between 2 and 150 characters."
                }
                else -> {
                    val data = hashMapOf<String, Any>(
                        "name" to name.text.toString(),
                        "grade" to grade.text.toString(),
                        "description" to desc.text.toString(),
                        "locationId" to locationModel.documentId.toString(),
                        "imageCount" to imageUris.count()
                    )
                    addProblemToFirestore(data)
                    Toast.makeText(view?.context, "Problem Added Successfully", Toast.LENGTH_SHORT).show()
                }
            }
        }
        return view
    }

    private fun uploadImage(uri: Uri, galleryIndex: Int, documentId: String) {
        val progressDialog = ProgressDialog(view?.context)
        progressDialog.setTitle("Uploading...")
        progressDialog.show()
        // Defining the child of storageReference
        val ref = storageReference.child("images/" + "problem_" + documentId + "_" + galleryIndex)
        ref.putFile(uri)
            .addOnSuccessListener {
                progressDialog.dismiss()
                Toast.makeText(view?.context, "Upload Successful", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e ->
                progressDialog.dismiss()
                Toast.makeText(view?.context, "Failed " + e.message, Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener {
                val taskSnapshot = it
                val progress: Double =
                    (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
                progressDialog.setMessage("Uploaded " + progress.toString() + "%")
            }
    }

    private fun addProblemToFirestore(data: HashMap<String, Any>) {
        val db = Firebase.firestore
        db.collection("problem")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    "ADD_PROBLEM_FRAGMENT",
                    "DocumentSnapshot written with ID: ${documentReference.id}"
                )
                problemDocumentId = documentReference.id
                if (imageUris.size > 0) {
                    for (i in 0 until imageUris.size) {
                        uploadImage(imageUris[i], i, documentReference.id)
                    }
                }
            }
            .addOnFailureListener { e ->
                Log.w("ADD_LOCATION_FRAGMENT", "Error adding document", e)
                Toast.makeText(view?.context, "Error. Please check your internet connection.", Toast.LENGTH_SHORT).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == RESULT_OK && requestCode == 1) {
            val selectedImageUri = intent?.data
            if (selectedImageUri != null) {
                imageUris.add(selectedImageUri)
            }
            val gallery = view?.findViewById<View>(R.id.gallery) as ViewPager
            val adapter = GalleryPagerAdapter(requireView().context, imageUris, null)
            gallery.adapter = adapter
        }
    }

    companion object {
        fun newInstance(locationModel: LocationModel): AddProblemFragment {
            val args = Bundle()
            val fragment = AddProblemFragment()
            args.putSerializable("locationModel", locationModel)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        val viewImageIntent = Intent(Intent.ACTION_VIEW, view.)
//        startActivity(viewImageIntent)
    }
}