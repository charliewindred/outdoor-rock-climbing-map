package com.example.outdoorclimbingmap.fragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.graphics.drawable.toBitmap
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.outdoorclimbingmap.GalleryPagerAdapter
import com.example.outdoorclimbingmap.ImageHelper
import com.example.outdoorclimbingmap.ListArrayAdapter
import com.example.outdoorclimbingmap.R
import com.example.outdoorclimbingmap.model.LocationModel
import com.example.outdoorclimbingmap.model.MediaModel
import com.example.outdoorclimbingmap.model.MediaRefModel
import com.example.outdoorclimbingmap.model.ProblemModel
import com.example.outdoorclimbingmap.services.LocationService
import com.example.outdoorclimbingmap.services.MediaRefService
import com.example.outdoorclimbingmap.services.MediaService
import com.example.outdoorclimbingmap.services.ProblemService
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class LocationFragment : Fragment(), AdapterView.OnItemClickListener{

    private lateinit var problemService: ProblemService
    private lateinit var locationService: LocationService
    private lateinit var mediaService: MediaService
    private lateinit var mediaRefService: MediaRefService
    private lateinit var storage: FirebaseStorage
    private lateinit var storageReference: StorageReference
    private lateinit var model: LocationModel

    private var imageViews = ArrayList<ImageView>()
    private var problems = ArrayList<ProblemModel>()

    companion object {
        fun newInstance(locationModel: LocationModel, isOffline: Boolean): LocationFragment {
            val args = Bundle()
            args.putSerializable("model",locationModel)
            args.putBoolean("isOffline", isOffline)
            val fragment = LocationFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_location, container, false)

        model = requireArguments().getSerializable("model") as LocationModel
        val isOffline = requireArguments().getBoolean("isOffline")

        problemService = ProblemService(view.context)
        locationService = LocationService(view.context)
        mediaService = MediaService(view.context)
        mediaRefService = MediaRefService(view.context)

        storage = FirebaseStorage.getInstance()
        storageReference = storage.reference

        val textLocationName : TextView = view.findViewById(R.id.textLocationName)
        val textLocationDescription : TextView = view.findViewById(R.id.textLocationDescription)
        val problemListView: ListView = view.findViewById(R.id.listViewProblems)
        textLocationName.text = model.name
        textLocationDescription.text = model.description

        val buttonAddProblem : FloatingActionButton = view.findViewById(R.id.buttonAddProblem)
        val buttonDownload: Button = view.findViewById(R.id.buttonDownload)
        if(isOffline) {
            buttonAddProblem.visibility = View.INVISIBLE
            buttonDownload.visibility = View.INVISIBLE
        }

        buttonAddProblem.setOnClickListener {
            val fragment = AddProblemFragment.newInstance(model)
            requireActivity().supportFragmentManager.beginTransaction().apply {
                replace(R.id.locationFragmentLayout, fragment)
                addToBackStack(null)
                commit()
            }
        }

        val problemStrings = ArrayList<String>()
        val listItemModelHashMap = HashMap<String, ProblemModel>()

        if(isOffline && model.id != null) {
            problems = problemService.getAllByLocationId(model.id!!)
            for (problem in problems) {
                val itemText = "   " + problem.name + " " + problem.grade
                listItemModelHashMap[itemText] = problem
                problemStrings.add(itemText)
            }

            val medias = mediaService.getAllByLocationId(model.id!!)
            if(medias.size > 0) {
                for (media in medias) {
                    val path = mediaRefService.getById(media.mediaRefId)?.mediaPath
                    val bitmap = ImageHelper.getImageFromInternalStorage(view.context, path!!)
                    val imageView = ImageView(view.context)
                    imageView.setImageBitmap(bitmap)
                    imageViews.add(imageView)
                    val gallery = view?.findViewById<View>(R.id.gallery) as ViewPager
                    val adapter = GalleryPagerAdapter(view.context, null, imageViews)
                    gallery.adapter = adapter
                }
            }
            val textNoProblems : TextView = view.findViewById(R.id.textNoProblems)
            if(problemStrings.isNotEmpty()){
                textNoProblems.visibility = View.INVISIBLE
                problemListView.adapter = ListArrayAdapter(view.context, R.layout.layout_problem_list, problemStrings)
                problemListView.setOnItemClickListener { _: AdapterView<*>, _: View, index: Int, _: Long ->
                    val selectedItemText = problemListView.getItemAtPosition(index).toString()
                    val fragment = ProblemFragment.newInstance(listItemModelHashMap[selectedItemText]!!, isOffline)
                    requireActivity().supportFragmentManager.beginTransaction().apply {
                        replace(R.id.locationFragmentLayout, fragment)
                        addToBackStack(null)
                        commit()
                    }
                }
            }
        } else {
            val db = Firebase.firestore
            db.collection("problem").whereEqualTo("locationId", model.documentId)
                .get()
                .addOnSuccessListener { result ->
                    for (document in result) {
                        val name = document.get("name").toString()
                        val desc = document.get("description").toString()
                        val grade = document.get("grade").toString()
                        val imageCount = document.get("imageCount").toString().toInt()
                        val problem = ProblemModel(
                            null,
                            null,
                            name,
                            desc,
                            grade,
                            model.documentId!!,
                            null,
                            imageCount
                        )
                        problem.documentId = document.id
                        if (model.id != null) {
                            problem.offlineLocationId = model.id
                        }
                        problems.add(problem)
                        val itemText = "   " + problem.name + " " + problem.grade
                        listItemModelHashMap[itemText] = problem
                        problemStrings.add(itemText)
                        problemListView.adapter = ListArrayAdapter(view.context, R.layout.layout_problem_list, problemStrings)
                        val textNoProblems : TextView = view.findViewById(R.id.textNoProblems)
                        if(problemStrings.isNotEmpty()) {
                            textNoProblems.visibility = View.INVISIBLE
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("Location Fragment", "Error getting documents.", exception)
                }

            if (model.imageCount!! > 0) {
                for (i in 0 until model.imageCount!!) {
                    val storageRef = FirebaseStorage.getInstance().reference
                    val imageView = ImageView(view?.context)
                    storageRef.child("images/" + "location_" + model.documentId + "_" + i).downloadUrl.addOnSuccessListener {
                        Glide.with(view?.context!!)
                            .asDrawable()
                            .load(it)
                            .override(2000)
                            .centerCrop()
                            .listener(object : RequestListener<Drawable> {
                                override fun onResourceReady(
                                    resource: Drawable?,
                                    model: Any?,
                                    target: Target<Drawable>?,
                                    dataSource: DataSource?,
                                    isFirstResource: Boolean
                                ): Boolean {
                                    imageView.setImageDrawable(resource)
                                    imageViews.add(imageView)
                                    val gallery = view.findViewById<View>(R.id.gallery) as ViewPager
                                    val adapter = GalleryPagerAdapter(view.context, null, imageViews)
                                    gallery.adapter = adapter
                                    return true
                                }

                                override fun onLoadFailed(
                                    e: GlideException?,
                                    model: Any?,
                                    target: Target<Drawable>?,
                                    isFirstResource: Boolean
                                ): Boolean {
                                    Log.e("Location Fragment", e.toString())
                                    return false
                                }
                            })
                            .into(imageView)
                    }
                }
            }

            problemListView.setOnItemClickListener { _: AdapterView<*>, _: View, index: Int, _: Long ->
                val selectedItemText = problemListView.getItemAtPosition(index).toString()
                if (listItemModelHashMap[selectedItemText] != null) {
                    val fragment = ProblemFragment.newInstance(
                        listItemModelHashMap[selectedItemText]!!,
                        isOffline
                    )
                    requireActivity().supportFragmentManager.beginTransaction().apply {
                        replace(R.id.locationFragmentLayout, fragment)
                        addToBackStack(null)
                        commit()
                    }
                }
            }
            buttonDownload.setOnClickListener {
                if (!locationService.locationExists(model.name)) {
                    model = locationService.add(model)

                    for (problem in problems) {
                        problem.offlineLocationId = model.id
                    }

                    //save location images to device and path to media ref table
                    if (model.imageCount!! > 0) {
                        for (i in 0 until model.imageCount!!) {
                            val imageName = "location_" + model.name.trim().lowercase().replace(" ", "_") + "_" + i
                            ImageHelper.saveToInternalStorage(
                                view.context,
                                imageViews[i].drawable.toBitmap(),
                                imageName
                            )
                            val mediaRef = mediaRefService.add(MediaRefModel(null, imageName))
                            mediaService.add(MediaModel(null, mediaRef.id!!, null, model.id!!))
                        }
                    }
                    Toast.makeText(view.context, "Download Successful", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(view.context, "Location Already Downloaded", Toast.LENGTH_SHORT).show()
                }
            }
        }
        return view
    }


    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //open image in image edit view
    }
}