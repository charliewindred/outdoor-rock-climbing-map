package com.example.outdoorclimbingmap.fragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.outdoorclimbingmap.GalleryPagerAdapter
import com.example.outdoorclimbingmap.ImageHelper
import com.example.outdoorclimbingmap.R
import com.example.outdoorclimbingmap.model.MediaModel
import com.example.outdoorclimbingmap.model.MediaRefModel
import com.example.outdoorclimbingmap.model.ProblemModel
import com.example.outdoorclimbingmap.services.MediaRefService
import com.example.outdoorclimbingmap.services.MediaService
import com.example.outdoorclimbingmap.services.ProblemService
import com.google.firebase.storage.FirebaseStorage

class ProblemFragment : Fragment(), AdapterView.OnItemClickListener {

    private lateinit var model: ProblemModel
    private lateinit var problemService: ProblemService
    private lateinit var mediaService: MediaService
    private lateinit var mediaRefService: MediaRefService

    private var imageViews = ArrayList<ImageView>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_problem, container, false)
        problemService = ProblemService(view.context)
        mediaService = MediaService(view.context)
        mediaRefService = MediaRefService(view.context)
        model = requireArguments().getSerializable("model") as ProblemModel
        val isOffline = requireArguments().getBoolean("isOffline")

        val textProblemName : TextView = view.findViewById(R.id.textProblemName)
        val textProblemDesc : TextView = view.findViewById(R.id.textProblemDescription)
        textProblemName.text = "${model.name} ${model.grade}"
        textProblemDesc.text = model.description

        if(isOffline){
            if(model.id != null) {
                val medias = mediaService.getAllByProblemId(model.id!!)
                if (medias.size > 0) {
                    for (media in medias) {
                        val path = mediaRefService.getById(media.mediaRefId)?.mediaPath
                        val bitmap = ImageHelper.getImageFromInternalStorage(view.context, path!!)
                        val imageView = ImageView(view.context)
                        imageView.setImageBitmap(bitmap)
                        imageViews.add(imageView)
                        val gallery = view?.findViewById<View>(R.id.gallery) as ViewPager
                        val adapter = GalleryPagerAdapter(view.context,null, imageViews)
                        gallery.adapter = adapter
                    }
                }
            }
        } else {
            if (model.imageCount!! > 0) {
                for (i in 0 until model.imageCount!!) {
                    val storageRef = FirebaseStorage.getInstance().reference
                    var imageView = ImageView(view?.context)
                    storageRef.child("images/" + "problem_" + model.documentId + "_" + i).downloadUrl.addOnSuccessListener {
                        Glide.with(view?.context!!)
                            .asDrawable()
                            .load(it)
                            .override(2000)
                            .centerCrop()
                            .listener(object : RequestListener<Drawable> {
                                override fun onResourceReady(
                                    resource: Drawable?,
                                    model: Any?,
                                    target: Target<Drawable>?,
                                    dataSource: DataSource?,
                                    isFirstResource: Boolean
                                ): Boolean {
                                    imageView.setImageDrawable(resource)
                                    imageViews.add(imageView)
                                    val gallery = view.findViewById<View>(R.id.gallery) as ViewPager
                                    val adapter = GalleryPagerAdapter(view.context,null, imageViews)
                                    gallery.adapter = adapter
                                    return true
                                }
                                override fun onLoadFailed(
                                    e: GlideException?,
                                    model: Any?,
                                    target: Target<Drawable>?,
                                    isFirstResource: Boolean
                                ): Boolean {
                                    Log.e("Problem Fragment", e.toString())
                                    return false
                                }
                            })
                            .into(imageView)
                    }
                }
            }
        }

        val buttonDownload : Button = view.findViewById(R.id.buttonDownload)
        if(isOffline){
            buttonDownload.visibility = View.INVISIBLE
        }
        buttonDownload.setOnClickListener {
            if(!problemService.problemExists(model.name)){
                if(model.offlineLocationId == null) {
                    Toast.makeText(view.context, "Please download the location first.", Toast.LENGTH_SHORT).show()
                } else {
                    model.id = problemService.add(model).id
                    //save location images to device and path to media ref table
                    if (model.imageCount!! > 0) {
                        for (i in 0 until model.imageCount!!) {
                            val imageName = "problem_" + model.name.trim().lowercase().replace(" ", "_") + "_" + i
                            ImageHelper.saveToInternalStorage(
                                view.context,
                                imageViews[i].drawable.toBitmap(),
                                imageName
                            )
                            val mediaRef = mediaRefService.add(MediaRefModel(null, imageName))
                            mediaService.add(MediaModel(null, mediaRef.id!!, model.id!!,null))
                        }
                    }
                    Toast.makeText(view.context, "Download Successful", Toast.LENGTH_SHORT).show()
                }
             } else {
                 Toast.makeText(view.context,"Problem Already Downloaded", Toast.LENGTH_SHORT).show()
             }
        }
        return view
    }

    companion object {
        fun newInstance(model: ProblemModel, isOffline: Boolean) : ProblemFragment {
            val args = Bundle()
            args.putSerializable("model", model)
            args.putBoolean("isOffline", isOffline)
            val fragment = ProblemFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("Not yet implemented")
    }
}
