package com.example.outdoorclimbingmap.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.outdoorclimbingmap.R
import com.example.outdoorclimbingmap.services.LocationService
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.outdoorclimbingmap.databinding.ActivityMapsBinding
import com.example.outdoorclimbingmap.fragments.AddLocationFragment
import com.example.outdoorclimbingmap.fragments.LocationFragment.Companion.newInstance
import com.example.outdoorclimbingmap.model.LocationModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.Marker
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.ktx.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import java.util.*
import kotlin.collections.ArrayList


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    companion object{
        private const val TAG = "MapsActivity"
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        const val DEFAULT_ZOOM = 15
        private const val KEY_CAMERA_POSITION = "camera_position"
        private const val KEY_LOCATION = "location"
    }

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationService: LocationService

    private var locationPermissionGranted : Boolean = false
    private var lastKnownLocation: Location? = null
    private var cameraPosition : CameraPosition? = null
    private var isOffline = false
    private var markerHashMap: HashMap<Marker, LocationModel> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationService = LocationService(this)

        isOffline = intent.getBooleanExtra("IS_OFFLINE", false)

        supportActionBar?.title = null

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (savedInstanceState != null) {
            lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            cameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        val offlineSwitch : SwitchCompat = findViewById(R.id.switchOffline)
        if(!offlineSwitch.isChecked && isOffline){
            offlineSwitch.isChecked = true
        }

        offlineSwitch.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                AlertDialog.Builder(this)
                    .setTitle("Offline Mode")
                    .setMessage("Would you like go offline?")
                    .setPositiveButton("Yes") { _, _ ->
                        isOffline = true
                        populateLocationsAndProblems()
                        mMap.setOnMapClickListener(null)
                        val buttonAddLocation = findViewById<FloatingActionButton>(R.id.buttonAddLocation)
                        buttonAddLocation.visibility = View.INVISIBLE
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                    .setNegativeButton("Cancel") { _, _ ->
                        offlineSwitch.isChecked = false
                    }
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
            } else {
                isOffline = false
                populateLocationsAndProblems()
                val buttonAddLocation = findViewById<FloatingActionButton>(R.id.buttonAddLocation)
                buttonAddLocation.visibility = View.VISIBLE
                mMap.uiSettings.isMyLocationButtonEnabled = true
                buttonAddLocation.setOnClickListener {
                    mMap.setOnMapClickListener(this)
                    Toast.makeText(this.applicationContext, "Click the map to select your location", Toast.LENGTH_LONG).show()
                }
                Toast.makeText(this.applicationContext, "Going online...", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        mMap.let { map ->
            outState.putParcelable(KEY_CAMERA_POSITION, map.cameraPosition)
            outState.putParcelable(KEY_LOCATION, lastKnownLocation)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menuLogout){
            Log.i(TAG, "Logout")
            auth.signOut()
            val logoutIntent = Intent(this, LoginActivity::class.java)
            logoutIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(logoutIntent)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun populateLocationsAndProblems(){
        val locations = ArrayList<LocationModel>()
        mMap.clear()

        if(isOffline){
            val offlineLocations = locationService.getAll()
            locations.addAll(offlineLocations)
            for(location in locations){
                val marker = mMap.addMarker(MarkerOptions().position(LatLng(location.latitude,location.longitude)).title(location.name))
                if (marker != null) {
                    markerHashMap[marker] = location
                }
            }
        } else {
            getLocationPermission()
            getDeviceLocation()
            updateLocationUI()

            val db = Firebase.firestore
            db.collection("location")
                .get()
                .addOnSuccessListener { result ->
                    for (document in result) {
                        val documentId = document.id
                        val name = document.get("name").toString()
                        val desc = document.get("description").toString()
                        val lat = document.get("latitude").toString().toDouble()
                        val long = document.get("longitude").toString().toDouble()
                        val imageCount = document.get("imageCount").toString().toInt()
                        val location = LocationModel(null, documentId, name, desc, lat, long, imageCount)

                        locations.add(location)
                    }

                    for(location in locations){
                        val marker = mMap.addMarker(MarkerOptions().position(LatLng(location.latitude,location.longitude)).title(location.name))
                        if (marker != null) {
                            markerHashMap[marker] = location
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents.", exception)
                }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        populateLocationsAndProblems()
        mMap.setOnMarkerClickListener(this)

        val buttonAddLocation = findViewById<FloatingActionButton>(R.id.buttonAddLocation)
        buttonAddLocation.setOnClickListener {
            mMap.setOnMapClickListener(this)
            Toast.makeText(this.applicationContext, "Click the map to select your location", Toast.LENGTH_LONG).show()
        }
    }

    override fun onMapClick(coordinates: LatLng) {
        mMap.setOnMapClickListener(null)
        val addLocationFragment = AddLocationFragment.newInstance(coordinates)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.locationFragmentLayout, addLocationFragment)
            addToBackStack(null)
            commit()
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val location = markerHashMap[marker]
        if(location != null) {
            val locationFragment = newInstance(
                LocationModel(
                    location.id,
                    location.documentId,
                    location.name,
                    location.description,
                    location.latitude,
                    location.longitude,
                    location.imageCount
                ), isOffline
            )
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.locationFragmentLayout, locationFragment)
                addToBackStack(null)
                commit()
        }
    }
        return false
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(this.applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
        updateLocationUI()
    }

    @SuppressLint("MissingPermission")
    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                lastKnownLocation = null
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lastKnownLocation!!.latitude, lastKnownLocation!!.longitude), DEFAULT_ZOOM.toFloat()))
                        }

                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    fun addMarker(location: LocationModel){
        val marker = mMap.addMarker(MarkerOptions().position(LatLng(location.latitude,location.longitude)).title(location.name))
        marker!!.position = marker.position
        markerHashMap[marker] = location
    }
}