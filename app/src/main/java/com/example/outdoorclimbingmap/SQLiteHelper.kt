package com.example.outdoorclimbingmap

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

private const val DATABASE_NAME: String = "climbs.db"
private const val DATABASE_VERSION: Int = 1

class SQLiteHelper private constructor(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private var instance: SQLiteHelper? = null

        @Synchronized
        fun getInstance(context: Context): SQLiteHelper? {
            if (instance == null) {
                instance = SQLiteHelper(context.applicationContext)
            }
            return instance
        }

        const val LOCATION_TABLE: String = "LOCATION_TABLE"
        const val COLUMN_LOCATION_DOCUMENT_ID: String = "LOCATION_DOCUMENT_ID"
        const val COLUMN_LOCATION_NAME: String = "LOCATION_NAME"
        const val COLUMN_LOCATION_DESCRIPTION: String = "LOCATION_DESCRIPTION"
        const val COLUMN_LOCATION_LATITUDE: String = "LOCATION_LATITUDE"
        const val COLUMN_LOCATION_LONGITUDE: String = "LOCATION_LONGITUDE"
        const val PROBLEM_TABLE: String = "PROBLEM_TABLE"
        const val COLUMN_PROBLEM_NAME: String = "PROBLEM_NAME"
        const val COLUMN_PROBLEM_DESCRIPTION: String = "PROBLEM_DESCRIPTION"
        const val COLUMN_PROBLEM_GRADE: String = "PROBLEM_GRADE"
        const val COLUMN_PROBLEM_LOCATION_ID: String = "PROBLEM_LOCATION_ID"
        const val MEDIA_REF_TABLE: String = "MEDIA_REF_TABLE"
        const val COLUMN_MEDIA_REF_PATH: String = "MEDIA_REF_PATH"
        const val MEDIA_TABLE: String = "MEDIA_TABLE"
        const val COLUMN_MEDIA_MEDIA_REF_ID: String = "MEDIA_MEDIA_REF_ID"
        const val COLUMN_MEDIA_PROBLEM_ID: String = "MEDIA_PROBLEM_ID"
        const val COLUMN_MEDIA_LOCATION_ID: String = "MEDIA_LOCATION_ID"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createLocationTableStatement: String =
            "CREATE TABLE " + LOCATION_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_LOCATION_DOCUMENT_ID + " TEXT, " +
                    COLUMN_LOCATION_NAME + " TEXT NOT NULL, " +
                    COLUMN_LOCATION_DESCRIPTION + " TEXT, " +
                    COLUMN_LOCATION_LATITUDE + " REAL NOT NULL, " +
                    COLUMN_LOCATION_LONGITUDE + " REAL NOT NULL);"

        val createMediaRefTableStatement: String =
            " CREATE TABLE " + MEDIA_REF_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_MEDIA_REF_PATH + " TEXT);"

        val createProblemTableStatement: String =
            " CREATE TABLE " + PROBLEM_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_PROBLEM_NAME + " TEXT NOT NULL, " +
                    COLUMN_PROBLEM_DESCRIPTION + " TEXT, " +
                    COLUMN_PROBLEM_GRADE + " TEXT, " +
                    COLUMN_PROBLEM_LOCATION_ID + " INT NOT NULL, " +
                    " FOREIGN KEY (" + COLUMN_PROBLEM_LOCATION_ID + ") REFERENCES " + LOCATION_TABLE + "(ID));"

        val createMediaTableStatement: String =
            " CREATE TABLE " + MEDIA_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_MEDIA_MEDIA_REF_ID + " INT NOT NULL, " +
                    COLUMN_MEDIA_PROBLEM_ID + " INT, " +
                    COLUMN_MEDIA_LOCATION_ID + " INT, " +
                    " FOREIGN KEY (" + COLUMN_MEDIA_MEDIA_REF_ID + ") REFERENCES " + MEDIA_REF_TABLE + "(ID)," +
                    " FOREIGN KEY (" + COLUMN_MEDIA_PROBLEM_ID + ") REFERENCES " + PROBLEM_TABLE + "(ID)," +
                    " FOREIGN KEY (" + COLUMN_MEDIA_LOCATION_ID + ") REFERENCES " + LOCATION_TABLE + "(ID));" +

        db?.execSQL(createLocationTableStatement)
        db?.execSQL(createMediaRefTableStatement)
        db?.execSQL(createProblemTableStatement)
        db?.execSQL(createMediaTableStatement)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}