package com.example.outdoorclimbingmap.model

import com.example.outdoorclimbingmap.interfaces.IEntity

data class MediaModel (
    var id: Int?,
    val mediaRefId: Int,
    val problemId: Int?,
    val locationId: Int?) : IEntity