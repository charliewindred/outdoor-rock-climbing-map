package com.example.outdoorclimbingmap.model

import com.example.outdoorclimbingmap.interfaces.IEntity

data class MediaRefModel(var id: Int?, val mediaPath: String) : IEntity