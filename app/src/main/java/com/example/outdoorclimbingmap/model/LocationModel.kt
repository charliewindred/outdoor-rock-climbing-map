package com.example.outdoorclimbingmap.model

import com.example.outdoorclimbingmap.interfaces.IEntity
import java.io.Serializable

data class LocationModel(
    var id: Int?,
    val documentId: String?,
    val name: String,
    val description: String?,
    val latitude: Double,
    val longitude: Double,
    val imageCount: Int?
    ) : IEntity, Serializable