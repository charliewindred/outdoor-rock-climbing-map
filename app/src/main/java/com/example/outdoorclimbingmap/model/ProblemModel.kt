package com.example.outdoorclimbingmap.model

import com.example.outdoorclimbingmap.interfaces.IEntity
import java.io.Serializable

data class ProblemModel(
    var id: Int?,
    var documentId : String?,
    val name: String,
    val description: String,
    val grade: String,
    val firestoreLocationId: String?,
    var offlineLocationId: Int?,
    var imageCount: Int?
    ) : IEntity, Serializable