package com.example.outdoorclimbingmap

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter

class ListArrayAdapter(context: Context, layoutResource: Int, listItems: ArrayList<String>) : ArrayAdapter<String>(context, layoutResource, listItems) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)
        if(position % 2 == 0){
            view.setBackgroundColor(Color.LTGRAY)
        }
        return view
    }

}