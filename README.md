# Outdoor Rock-Climbing Map 

# Project Overview
Outdoor Climbing Map is a community repository of outdoor rock-climbing locations 
and problems. Users can view a map of climbing locations submitted by other users and can also submit 
their own. A location consists of a name, description, a collection of submitted images, and a collection 
of problems. Users can submit their own problem for any location. A problem consists of a name, 
description, grade, and a collection of images. Users are encouraged to present their information with 
annotated images rather than long descriptions. The application has an offline mode, where users can 
view the locations and problems that they have downloaded to their device. 

The application is written in Kotlin within the Android Framework. Project dependencies include the 
Google Maps SDK, Firebase for its cloud-based database and an SQLite database for the local offline storage. 
The project was inspired by climbing training board apps like MoonBoard, which allows users to 
submit their own problems for a standardized board.
